%global short_name commons-ognl
Name:                apache-%{short_name}
Version:             3.0.2
Release:             1
Summary:             Object Graph Navigation Library
License:             ASL 2.0
URL:                 http://commons.apache.org/ognl/
Source0:             http://apt.linuxfans.org/magic/3.0/sources/SOURCES.a/apache-commons-ognl/apache-commons-ognl-3.0.2.tar.xz
BuildArch:           noarch
BuildRequires:       jna maven-local mvn(javassist:javassist) mvn(junit:junit)
BuildRequires:       mvn(org.apache.commons:commons-parent:pom:) mvn(org.easymock:easymock)
%description
OGNL is an expression language for getting and setting properties of
Java objects, plus other extras such as list projection and selection
and lambda expressions.

%package javadoc
Summary:             API documentation for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q
%pom_remove_plugin com.mycila.maven-license-plugin:maven-license-plugin
%pom_remove_plugin org.apache.maven.plugins:maven-clean-plugin
rm -r src/test/java/org/apache/commons/ognl/test/ArithmeticAndLogicalOperatorsTest.java \
  src/test/java/org/apache/commons/ognl/test/ArrayElementsTest.java \
  src/test/java/org/apache/commons/ognl/test/ASTPropertyTest.java \
  src/test/java/org/apache/commons/ognl/test/CollectionDirectPropertyTest.java \
  src/test/java/org/apache/commons/ognl/test/ConstantTest.java \
  src/test/java/org/apache/commons/ognl/test/MethodTest.java \
  src/test/java/org/apache/commons/ognl/test/PropertyArithmeticAndLogicalOperatorsTest.java \
  src/test/java/org/apache/commons/ognl/test/PropertyTest.java \
  src/test/java/org/apache/commons/ognl/test/enhance/TestExpressionCompiler.java
%mvn_file :%{short_name} %{short_name} %{name}

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE.txt NOTICE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt NOTICE.txt

%changelog
* Mon Aug 17 2020 chengzihan <chengzihan2@huawei.com> - 3.0.2-1
- Package init
